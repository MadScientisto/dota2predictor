import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_fscore_support

#Load Data
train = np.genfromtxt("../data/train.csv",delimiter=',')
validation = np.genfromtxt("../data/validation.csv",delimiter=',')
test = np.genfromtxt("../data/test.csv",delimiter=',')

train_Y = train[:,0]
train_X = train[:,1:]

val_Y = validation[:,0]
val_X = validation[:,1:]

test_Y = test[:,0]
test_X = test[:,1:]

#Build Model
regularizer = None

input_layer = tf.keras.layers.Dense(128, activation='relu', kernel_regularizer=regularizer,  input_shape=(114,))
base_layer = tf.keras.layers.Dense(128, activation='relu', kernel_regularizer=regularizer)
output_layer = tf.keras.layers.Dense(1, activation='relu', kernel_regularizer=regularizer)

dota2_model = tf.keras.Sequential([
    input_layer,
    base_layer,
    base_layer,
    base_layer,
    base_layer,
    output_layer
])



dota2_model.compile(optimizer='adam',
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=['accuracy']
)

#Train model
history = dota2_model.fit(train_X, train_Y, 
                validation_data=(val_X, val_Y), 
                epochs=50, batch_size=512,
                callbacks=[tf.keras.callbacks.EarlyStopping(
                           monitor='val_accuracy', min_delta=0,
                           patience=10, restore_best_weights=True)])

#Save model
dota2_model.save("../dota2_model")

#Test model
test_pred = dota2_model.predict(test_X)
test_pred = np.where(test_pred > 0, 1, 0)

confusion_matrix = tf.math.confusion_matrix(test_Y, test_pred, num_classes=2)
print(confusion_matrix)

def plot_acc(history):

    train_acc = history.history["accuracy"]
    val_acc = history.history["val_accuracy"]
    epochs = range(1,len(history.history["accuracy"])+1)

    plt.plot(epochs, train_acc, color="red", label="train_acc")
    plt.plot(epochs, val_acc, color="blue", label="val_acc")

    best_val_acc = max(val_acc)
    plt.plot(epochs, np.repeat(best_val_acc, len(epochs)), color="black", linestyle="dashed")

    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")

    plt.legend()

    plt.show()

plot_acc(history)

