"""
    Script to clean the dota2 dataset
    Usage: python3 data_cleaner.py <original_dataset_filepath> <clean_dataset_filepath>
"""

import sys
import numpy as np

def check_heroes(row):

    #Check for missing values or extra in hero vector
    team1_count = np.count_nonzero(row[2:] == 1)
    team2_count = np.count_nonzero(row[2:] == -1)

    if team1_count == team2_count and team2_count == 5:
        return True
    else:
        return False 
        
def data_cleaner(original_data):

    #Remove ClusterID and Game Mode columns
    clean_data = np.delete(original_data, np.s_[1:3], 1)

    #Check values - All values where valid so I did not need to remove any rows
    for row in clean_data:

        #Check if result column is valid
        if not (row[0]!=1 or row[0]!=-1):
            #Handle non valid result
            print("Not valid result.")

        #Check if game type column is valid
        if not (row[1]!=2 or row[1]!=3):
            #Handle non valid game type
            print("Not valid game type.")

        #Check for missing or extra heroes
        if not check_heroes(row):
            #Handle missing values
            print("Missing or extra heroes.")

    #Binarize Result and  Game Type columns
    clean_data[:,0] = np.where(clean_data[:,0]==1, 1, 0)
    clean_data[:,1] = np.where(clean_data[:,1]==2, 1, 0)

    return clean_data

def main():

    original_filepath = sys.argv[1]
    clean_filepath = sys.argv[2]

    print("Loading data. . .")
    original_data = np.genfromtxt(original_filepath,delimiter=',')

    print("Cleaning data. . .")
    clean_data = data_cleaner(original_data)

    print("Writing data to " + clean_filepath + ". . .")
    np.savetxt(clean_filepath, clean_data, fmt='%i', delimiter=',')

if __name__ == "__main__":
    main()