"""
    Script to split the cleaned dota2 dataset
    Run after data_cleaner.py
    Usage: python3 data_cleaner.py <clean_dataset_filepath> <train_dataset_filepath> <validation_dataset_filepath> <test_dataset_filepath>
"""

import sys
import numpy as np

def random_split(data, train_per, val_per, test_per):

    random_perm = np.random.permutation(data)

    train_count = int(np.floor(data.shape[0]*train_per))
    val_count = int(np.floor(data.shape[0]*val_per))

    train_data = data[:train_count]
    val_data = data[train_count:train_count+val_count]
    test_data = data[train_count+val_count:]

    return train_data, val_data, test_data

def main():

    data_filepath = sys.argv[1]
    train_filepath = sys.argv[2]
    validation_filepath = sys.argv[3]
    test_filepath = sys.argv[4]

    print("Loading data. . .")
    data = np.genfromtxt(data_filepath,delimiter=',')

    #Split 50% train, 25% validation, 25% test
    train, val, test = random_split(data, 0.5, 0.25, 0.25)

    #Save files
    print("Writing data. . .")
    np.savetxt(train_filepath, train, fmt='%i', delimiter=',')
    np.savetxt(validation_filepath, val, fmt='%i', delimiter=',')
    np.savetxt(test_filepath, test, fmt='%i', delimiter=',')


if __name__ == "__main__":
    main()